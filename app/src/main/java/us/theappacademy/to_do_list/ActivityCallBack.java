package us.theappacademy.to_do_list;

public interface ActivityCallback {
    void onPostSelected(int pos);
}
